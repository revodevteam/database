<?php
namespace Framework\Database\Connections;

use mysqli;
use Framework\Database\Configuration\Configuration;

interface ConnectionInterface
{

    public function connect(Configuration $configuration): mysqli;
}
