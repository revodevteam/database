<?php
namespace Framework\Database\Connections;

use mysqli;
use Framework\Database\Configuration\Configuration;
use Framework\Database\Connections\ConnectionInterface;
use Framework\Database\Exceptions\MySqliConnectionException;

class MySqliConnection implements ConnectionInterface
{

    protected static $connection = [];

    /**
     * Connect to MySQL 
     * 
     * @param Configuration $configuration
     * @return mysqli
     * @throws MySqliConnectionException
     */
    public function connect(Configuration $configuration): mysqli
    {
        if (!empty(self::$connection) || isset(self::$connection[$configuration->database])) {
            return self::$connection[$configuration->database];
        }

        $connection = self::$connection[$configuration->database] = new mysqli($configuration->host, $configuration->user, $configuration->password, $configuration->database);

        if (!empty($connection->connect_errno)) {
            throw new MySqliConnectionException($connection->connect_error);
        }

        if ($configuration->characterSet !== null) {
            $connection->set_charset($configuration->characterSet);
        }



        return $connection;
    }

    public function disconnect(Configuration $configuration)
    {
        $this->getConnection($configuration)->close();
    }

    public function getConnection(Configuration $configuration)
    {
        if (!empty(self::$connection) && isset(self::$connection[$configuration->database])) {
            return self::$connection[$configuration->database];
        }

        return false;
    }

    public static function terminate()
    {
        if (!empty(self::$connection)) {
            foreach (self::$connection as $connection) {
                $connection->close();
            }
            return true;
        }

        return false;
    }
}
