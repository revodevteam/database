<?php

namespace Framework\Database\Stacky;

use Framework\Helpers\Collection;

class RowCollection extends Collection {

    public function returnAllID()
    {
        return array_column($this->collection, 'id');
    }

}
